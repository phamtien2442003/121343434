import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while True:
    _, frame = cap.read()
    frame = cv2.flip(frame,1)
    img = frame
    n = img.shape[1]
    m = img.shape[0]

    start_point = (img.shape[1]//2, 0)
    end_point = (img.shape[1]//2, img.shape[0])
    img = cv2.line(img, start_point, end_point, (0,255,0), 2)
    copy1 = np.zeros_like(img)
    copy1 = np.zeros_like(img)
    copy1 = img[0:img.shape[0], 0:img.shape[1]//2] 
    copy2 = img[0:img.shape[0], img.shape[1]//2:860]
    l_c = np.array([61, 116, 251])
    u_c = np.array([224, 210, 255])
    red_mask = cv2.inRange(copy2, l_c, u_c)
    filter_mask = cv2.bitwise_and(copy2, copy2, mask = red_mask)
    cv2.imshow('fm', filter_mask)
    cv2.imshow('rm', red_mask)
    _, thresh = cv2.threshold(red_mask, 127, 255, cv2.THRESH_BINARY)
    erode = cv2.erode(thresh, (21,21))
    cv2.imshow('erode', erode)
    contours ,_ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for contour in contours:
        (x, y, w, h) = cv2.boundingRect(contour)
        if cv2.contourArea(contour) > 5000:
            print('1')
            cv2.rectangle(copy2, (x,y), (x+w, y+h), (0,255,0), 2)
    for i in range(m):
        for j in range(n):
            if (j<n//2):
                img[i][j] = copy1[i][j]
            else:
                img[i][j] = copy2[i][j-320]
    cv2.imshow('f_s', img)
    key = cv2.waitKey(1)
    if key == 27:
        break 
    

cap.release()
cv2.destroyAllWindows()