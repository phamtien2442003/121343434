import cv2
import numpy as np

def nothing(x):
    pass
cap = cv2.VideoCapture(0)
#img = cv2.imread("6dagiac.jpg")
cv2.namedWindow("Tracking")
#tạo trackbar 
cv2.createTrackbar("LH", "Tracking", 0, 255, nothing)
cv2.createTrackbar("LS", "Tracking", 0, 255, nothing)
cv2.createTrackbar("LV", "Tracking", 0, 255, nothing)

cv2.createTrackbar("UH", "Tracking", 255, 255, nothing)
cv2.createTrackbar("US", "Tracking", 255, 255, nothing)
cv2.createTrackbar("UV", "Tracking", 255, 255, nothing)

while True:
    _, frame = cap.read()
    frame = cv2.flip(frame, 1)
    img = frame
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    #lấy giá trị của trackbar
    l_h = cv2.getTrackbarPos("LH", "Tracking")
    l_s = cv2.getTrackbarPos("LS", "Tracking")
    l_v = cv2.getTrackbarPos("LV", "Tracking")

    u_h = cv2.getTrackbarPos("UH", "Tracking")
    u_s = cv2.getTrackbarPos("US", "Tracking")
    u_v = cv2.getTrackbarPos("UV", "Tracking")

    l_c = np.array([l_h, l_s, l_v])
    u_c = np.array([u_h, u_s, u_v])

    mask = cv2.inRange(hsv, l_c, u_c)

    res = cv2.bitwise_and(frame, frame, mask=mask)
    cv2.imshow("mask", mask)
    # cv2.imshow("res", res)

    key = cv2.waitKey(1)
    if key == 27:
        break

cap.release()
cv2.destroyAllWindows()
