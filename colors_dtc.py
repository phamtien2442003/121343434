import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while True:
    _, frame = cap.read()
    frame = cv2.flip(frame, 1)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    l_r = np.array([3, 150, 161])
    u_r = np.array([9, 187, 255])

    l_g = np.array([53, 36, 158])
    u_g = np.array([84, 200, 255])

    l_b = np.array([86, 43, 158])
    u_b = np.array([134, 174, 255])
    
    mask_red = cv2.inRange(hsv, l_r, u_r)
    red_pix = cv2.countNonZero(mask_red)

    mask_green = cv2.inRange(hsv, l_g, u_g)
    green_pix = cv2.countNonZero(mask_green)

    mask_blue = cv2.inRange(hsv, l_b, u_b)
    blue_pix = cv2.countNonZero(mask_blue)
    if (red_pix > 5000):
        print('1')
        contours, _ = cv2.findContours(mask_red, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        for contour in contours:
            if cv2.contourArea(contour) > 5000:
                (x, y, w, h) = cv2.boundingRect(contour)
                centroid_x = x+w //2, centroid_y = y+h //2
                if centroid_x - pre_centroidx < -20 :
                    print('Len tren')
                cv2.rectangle(frame, (x,y), (x+w, y+h), (0,255,0), 2)
                cv2.putText(frame, 'Mau do', (x,y), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 1, cv2.LINE_AA)  
    if (green_pix > 5000):
        print('2')
        contours, _ = cv2.findContours(mask_green, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        for contour in contours:
            if cv2.contourArea(contour) > 5000:
                (x, y, w, h) = cv2.boundingRect(contour)
                cv2.rectangle(frame, (x,y), (x+w, y+h), (0,255,0), 2)
                cv2.putText(frame, 'Mau xanh la cay', (x,y), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 1, cv2.LINE_AA)  
    if (blue_pix > 5000):
        print('3')
        contours, _ = cv2.findContours(mask_blue, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        for contour in contours:
            if cv2.contourArea(contour) > 5000:
                (x, y, w, h) = cv2.boundingRect(contour)
                cv2.rectangle(frame, (x,y), (x+w, y+h), (0,255,0), 2)
                cv2.putText(frame, 'Mau xanh duong', (x,y), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1, cv2.LINE_AA)  
    cv2.imshow('frame', frame)
    key = cv2.waitKey(1)
    if key == 27:
        break 
    

cap.release()
cv2.destroyAllWindows()

